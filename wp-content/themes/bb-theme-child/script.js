jQuery(function($) {
    $(document).ready(function() {
        //$(".homePopularStyle .uabb-tabs .uabb-tabs-nav").prependTo( ".homePopularStyle .uabb-content-wrap .uabb-content-current .tabContent .fl-col-content .uabb-module-content" );	

        $(".homePopularStyle .uabb-tabs .uabb-tabs-nav ul > li .uabb-tab-link").click(appendtorow);

        if ($(".homePopularStyle")) {
            appendtorow();
        }

        function appendtorow() {
            $(".showImgWrap > .fl-col-content ").animate({
                width: "toggle"
            });
            setTimeout(() => {
                let url = $(".homePopularStyle .uabb-content-wrap .uabb-content-current .fl-row-content-wrap .fl-col-group > div:nth-child(1) .fl-col-content").css('background-image');
                $(".showImgWrap > .fl-col-content ").css("background-image", url);
                $(".showImgWrap > .fl-col-content ").animate({
                    width: "toggle"
                });
            }, 1000);
        }

        $(".tax_img").hover(function() {
            $(".tax_img").hide();
            var data_img = $(this).attr("data-img");

            $(".tax_img." + data_img).show();
        });
    });
});