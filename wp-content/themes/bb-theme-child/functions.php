<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
    // wp_enqueue_script("bootstrap-script","http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js","","",1);
});


// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );

// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');

function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}


add_action('wp_ajax_myfilter', 'gallery_filter_function'); // wp_ajax_{ACTION HERE} 
add_action('wp_ajax_nopriv_myfilter', 'gallery_filter_function');
 
function gallery_filter_function(){

    // for taxonomies / categories
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    $args = array(
        'post_type' => 'gallery',
        'posts_per_page' => 9,
        'paged' => $paged
    );
	if($_POST['roomfilter'] !="" ){
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'room',
				'field' => 'id',
				'terms' => $_POST['roomfilter']
			)
		);
    }
    if( $_POST['stylesfilter'] !="" ){
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'styles',
				'field' => 'id',
				'terms' => $_POST['stylesfilter']
			)
		);
    }
    if( $_POST['roomfilter'] !="" && $_POST['stylesfilter'] !="" ){
		$args['tax_query'] = array(
            'relation' => 'AND',
			array(
				'taxonomy' => 'room',
				'field' => 'id',
				'terms' => $_POST['roomfilter']
            ),
            array(
				'taxonomy' => 'styles',
				'field' => 'id',
				'terms' => $_POST['stylesfilter']
			)
		);
    }

	$custom_query = new WP_Query( $args );
 ?>
		<?php if($custom_query->have_posts()) { ?>
					<div class="gallery_list_holder row">
					<?php
						$popup_id = 0;
						while($custom_query->have_posts()) :
						$custom_query->the_post();
					?>
					<div class="col-md-4">
						<div class="gallery-item">
							<div class="gallery-image" >
								<a href="javascript:void(0)" data-toggle="modal" data-target="#gallery-modal-<?php echo $popup_id; ?>" >
									<?php //echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
									<div class="fearured_img_holder">
										<?php the_post_thumbnail('full'); ?>
										<span class="fa fa-search-plus" aria-hidden="true"></span>
									</div>
									<!-- <h5 id="gallery-modal-label-<?php echo $popup_id; ?>"><a href="<?php the_permalink(); ?>" ><?php the_title(); ?></a></h5> -->
								</a>
							</div>
							<div class="modal fade" id="gallery-modal-<?php echo $popup_id; ?>"  role="dialog" >
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
										</div>
										<div class="modal-body">
											<div class="popup-holder">
												<?php
													$images = get_field('gallery_images');
													$size = 'full'; // (thumbnail, medium, large, full or custom size)
												?>
												<!-- slider start here -->

												<div class="carouselWrapper">
													<div class="carouselOne owl-carousel">
														<?php   if( $images ):
																foreach( $images as $image_id ):
																	if($image_id["url"] != "") {
																	if($image_id["alt"]) { $img_alt = $image_id["alt"]; } else { $img_alt = $image_id["title"]; }
														?>
																<div class="item" data-hash="<?php echo $img_alt; ?>">
																		<img src="<?php echo $image_id["url"]; ?>" alt="<?php echo $img_alt; ?>" />
																	<?php } ?>
																</div>
															<?php
																endforeach;
																endif;
															?>
													</div> 
												</div> 

												<div class="sliderThubnail">	
													<h5 style="padding-left: 20px!important; margin: 0px!important;">More</h5>			
													<div class="carouselTwo  owl-carousel">
														<?php   if( $images ):
																foreach( $images as $image_id ):
														?>
																<div class="item">
																	<?php
																		if($image_id["url"] != "") {
																		if($image_id["alt"]) { $img_alt = $image_id["alt"]; } else { $img_alt = $image_id["title"]; }
																	?>
																		<a class="button secondary url" href="#<?php echo $img_alt; ?>">
																			<img src="<?php echo $image_id["url"]; ?>" alt="<?php echo $img_alt; ?>" />
																		</a>
																	<?php } ?>
																</div>
															<?php
																endforeach;
																endif;
															?>
													</div> 				
												</div>				
												<!-- slider start end -->
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										</div>						
									</div>
								</div>
							</div>
						</div>
					</div>								
					<?php
						$popup_id++;
						endwhile;
					?>
					</div>
					<div class="col-md-12 text-center">
						<div class="pagination">
							<?php 
								echo paginate_links( array(
									'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
									'total'        => $custom_query->max_num_pages,
									'current'      => max( 1, get_query_var( 'paged' ) ),
									'format'       => '?paged=%#%',
									'show_all'     => false,
									'type'         => 'plain',
									'end_size'     => 2,
									'mid_size'     => 1,
									'prev_next'    => true,
									'prev_text'    => sprintf( '<i></i> %1$s', __( 'Newer Posts', 'text-domain' ) ),
									'next_text'    => sprintf( '%1$s <i></i>', __( 'Older Posts', 'text-domain' ) ),
									'add_args'     => false,
									'add_fragment' => '',
								) );
							?>
						</div>
					</div>
					<?php } else { ?>
						<div class="no-gallery-found">no results found - todo: add this message from admin</div>
					<?php }  ?>
					</div>
					
				</div><?php

 
	die();
}

function room_menu_items_function() {
    if( $terms = get_terms( array( 'taxonomy' => 'room', 'orderby' => 'name' ,'hide_empty' => false))  ) : 
 
        $return_string = '<ul>';
        foreach ( $terms as $term ) :
            $return_string .= '<li class="tax_li" data-img="'.sanitize_title($term->name).'"><a href="/galleries/?room='.$term->term_id.'">' . $term->name . '</a></li>'; // ID of the category as the value of an option
        endforeach;
        $return_string .='</ul>';
    endif;
    wp_reset_query();
    return $return_string;
 }
 add_shortcode('ROOM-MENU-ITEMS', 'room_menu_items_function');

 
function styles_menu_items_function() {
    if( $terms = get_terms( array( 'taxonomy' => 'styles', 'orderby' => 'name' ,'hide_empty' => false))  ) : 
 
        $return_string = '<ul>';
        foreach ( $terms as $term ) :
            $return_string .= '<li class="tax_li" data-img="'.sanitize_title($term->name).'"><a href="/galleries/?styles='.$term->term_id.'">' . $term->name . '</a></li>'; // ID of the category as the value of an option
        endforeach;
        $return_string .='</ul>';
    endif;
    wp_reset_query();
    return $return_string;
 }
 add_shortcode('STYLES-MENU-ITEMS', 'styles_menu_items_function');

 
function menu_image_items_function() {
   
    $return_string = '<div class="">';
    $terms = get_terms( array( 'taxonomy' => 'styles', 'orderby' => 'name' ,'hide_empty' => false));
    $img_url ="/wp-content/uploads/2020/06/tlr6-highlands-east-ridgecrest_family-room-300x200-1.jpg";
    $return_string .= '<div class=" tax_img show_first"> <img width="300" src="'.$img_url.'"><p class="term_title">Gellery</p></div>'; 
    if( $terms) : 
        foreach ( $terms as $term ) :
            $img_url = get_field('tax_image' , $term->term_id) ? get_field('tax_image' , $term->term_id): "/wp-content/uploads/2020/06/tlr6-highlands-east-ridgecrest_family-room-300x200-1.jpg";
            $return_string .= '<div class=" tax_img '.sanitize_title($term->name).'"> <img width="300" src="'.$img_url.'"><p class="term_title">' . $term->name . '</p></div>'; // ID of the category as the value of an option
        endforeach;
    endif;

    $terms = get_terms( array( 'taxonomy' => 'room', 'orderby' => 'name' ,'hide_empty' => false));
    if( $terms) : 
        foreach ( $terms as $term ) :
            $img_url = get_field('tax_image' , $term->term_id) ? get_field('tax_image' , $term->term_id): "/wp-content/uploads/2020/06/tlr6-highlands-east-ridgecrest_family-room-300x200-1.jpg";
            $return_string .= '<div class=" tax_img '.sanitize_title($term->name).'"> <img width="300" src="'.$img_url.'"><p class="term_title">' . $term->name . '</p></div>'; // ID of the category as the value of an option
        endforeach;
    endif;
    $return_string .='</div>';
    wp_reset_query();
    return $return_string;
 }
 add_shortcode('MENU-IMAGE-ITEMS', 'menu_image_items_function');

 function filter_site_upload_size_limit( $size ) {
    // Set the upload size limit to 10 MB for users lacking the 'manage_options' capability.
    if ( ! current_user_can( 'manage_options' ) ) {
        
        $size = 1024 * 1000 * 256;
    }
    return $size;
}
add_filter( 'upload_size_limit', 'filter_site_upload_size_limit', 20 );