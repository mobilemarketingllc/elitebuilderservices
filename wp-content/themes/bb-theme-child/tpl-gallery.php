<?php

/*
Template Name: Gallery Template
*/

get_header();

?>
<link href="<?php echo get_stylesheet_directory_uri(); ?>/slider/css/style.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo get_stylesheet_directory_uri(); ?>/slider/css/owl.theme.default.min.css" rel="stylesheet" type="text/css"/>
<div class="fl-content-full container gallery-template">
	<div class="row">
		<div class="fl-content col-md-12">
			<?php // Gallery Query Start Here ?>
			<?php
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$args = array(
					'post_type' => 'gallery',
					'posts_per_page' => 9,
					'paged' => $paged
				);

				   
	if($_GET['room'] !="" ){
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'room',
				'field' => 'id',
				'terms' => $_GET['room']
			)
		);
    }
    if( $_GET['styles'] !="" ){
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'styles',
				'field' => 'id',
				'terms' => $_GET['styles']
			)
		);
    }
    if( $_GET['room'] !="" && $_GET['styles'] !="" ){
		$args['tax_query'] = array(
            'relation' => 'AND',
			array(
				'taxonomy' => 'room',
				'field' => 'id',
				'terms' => $_GET['room']
            ),
            array(
				'taxonomy' => 'styles',
				'field' => 'id',
				'terms' => $_GET['styles']
			)
		);
    }

				$custom_query = new WP_Query( $args );
				
			?>
			<main id="main" class="site-main shadowBottom" role="main">
				<div class="container">
				
				<h1 class="galleryTitle">Find your Design Inspiration</h1>
				<form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="filter">

				<?php
				if( $terms = get_terms( array( 'taxonomy' => 'styles', 'orderby' => 'name' ,'hide_empty' => false) ) ) : 
		
					echo '<select name="stylesfilter"><option value="">Select Styles</option>';
					foreach ( $terms as $term ) :
						echo '<option value="' . $term->term_id . '">' . $term->name . '</option>'; // ID of the category as the value of an option
					endforeach;
					echo '</select>';
				endif;
			?>			
			<?php
				if( $terms = get_terms( array( 'taxonomy' => 'room', 'orderby' => 'name' ,'hide_empty' => false))  ) : 
		
					echo '<select name="roomfilter"><option value="">Select Room</option>';
					foreach ( $terms as $term ) :
						echo '<option value="' . $term->term_id . '">' . $term->name . '</option>'; // ID of the category as the value of an option
					endforeach;
					echo '</select>';
				endif;
			?>

	<!-- <button>Apply filter</button> -->
	<input type="hidden" name="action" value="myfilter">
</form>

<div id="response">

					<?php if($custom_query->have_posts()) { ?>
					<div class="gallery_list_holder row">
					<?php
						$popup_id = 0;
						while($custom_query->have_posts()) :
						$custom_query->the_post();
					?>
					<div class="col-md-4">
						<div class="gallery-item">
							<div class="gallery-image" >
								<a href="javascript:void(0)" data-toggle="modal" data-target="#gallery-modal-<?php echo $popup_id; ?>" >
									<?php //echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
									<div class="fearured_img_holder">
										<?php the_post_thumbnail('full'); ?>
										<span class="fa fa-search-plus" aria-hidden="true"></span>
									</div>
									<!-- <h5 id="gallery-modal-label-<?php echo $popup_id; ?>"><a href="<?php the_permalink(); ?>" ><?php the_title(); ?></a></h5> -->
								</a>
							</div>
							<div class="modal fade" id="gallery-modal-<?php echo $popup_id; ?>"  role="dialog" >
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
										</div>
										<div class="modal-body">
											<div class="popup-holder">
												<?php
													$images = get_field('gallery_images');
													$size = 'full'; // (thumbnail, medium, large, full or custom size)
												?>
												<!-- slider start here -->

												<div class="carouselWrapper">
													<div class="carouselOne owl-carousel">
														<?php   if( $images ):
																foreach( $images as $image_id ):
																	if($image_id["url"] != "") {
																	if($image_id["alt"]) { $img_alt = $image_id["alt"]; } else { $img_alt = $image_id["title"]; }
														?>
																<div class="item" data-hash="<?php echo $img_alt; ?>">
																		<img src="<?php echo $image_id["url"]; ?>" alt="<?php echo $img_alt; ?>" />
																	<?php } ?>
																</div>
															<?php
																endforeach;
																endif;
															?>
													</div> 
												</div> 

												<div class="sliderThubnail">	
													<h5 style="padding-left: 20px!important; margin: 0px!important;">More</h5>			
													<div class="carouselTwo  owl-carousel">
														<?php   if( $images ):
																foreach( $images as $image_id ):
														?>
																<div class="item">
																	<?php
																		if($image_id["url"] != "") {
																		if($image_id["alt"]) { $img_alt = $image_id["alt"]; } else { $img_alt = $image_id["title"]; }
																	?>
																		<a class="button secondary url" href="#<?php echo $img_alt; ?>">
																			<img src="<?php echo $image_id["url"]; ?>" alt="<?php echo $img_alt; ?>" />
																		</a>
																	<?php } ?>
																</div>
															<?php
																endforeach;
																endif;
															?>
													</div> 				
												</div>				
												<!-- slider start end -->
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										</div>						
									</div>
								</div>
							</div>
						</div>
					</div>								
					<?php
						$popup_id++;
						endwhile;
					?>
					</div>
					<div class="col-md-12 text-center">
						<div class="pagination">
							<?php 
								echo paginate_links( array(
									'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
									'total'        => $custom_query->max_num_pages,
									'current'      => max( 1, get_query_var( 'paged' ) ),
									'format'       => '?paged=%#%',
									'show_all'     => false,
									'type'         => 'plain',
									'end_size'     => 2,
									'mid_size'     => 1,
									'prev_next'    => true,
									'prev_text'    => sprintf( '<i></i> %1$s', __( 'Newer Posts', 'text-domain' ) ),
									'next_text'    => sprintf( '%1$s <i></i>', __( 'Older Posts', 'text-domain' ) ),
									'add_args'     => false,
									'add_fragment' => '',
								) );
							?>
						</div>
					</div>
					<?php } else { ?>
						<div class="no-gallery-found">no results found - todo: add this message from admin</div>
					<?php }  ?>
					</div>
					
				</div>
			</main>
			<?php // Gallery Query End Here ?>
			<div class="col-lg-12">
<?php echo do_shortcode("[fl_builder_insert_layout slug='gallery-page-product-row']"); ?>
</div>		
		</div>
	</div>
</div>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/slider/js/owl.carousel.min.js"></script>
<script>
jQuery(document).ready(function(){

	function sliderShow(){
		jQuery('.carouselOne').owlCarousel({
			items:1,
			loop:true,
			margin:10,
			nav:true,
			callbacks: true,
			URLhashListener: true,
			autoplayHoverPause: true,
			startPosition: 'URLHash'
		});

		jQuery('.sliderThubnail > .carouselTwo').owlCarousel({
			loop:false,
			margin: 8,
			callbacks: true,
			responsive:{
				0:{
					items:1
				},
				600:{
					items:3
				},
				1000:{
					items:5
				}
			}
		});
	}
	sliderShow();
	

	jQuery(function($){
	jQuery('#filter select').change(function(){
		var filter = jQuery('#filter');
		jQuery.ajax({
			url:filter.attr('action'),
			data:filter.serialize(), // form data
			type:filter.attr('method'), // POST
			beforeSend:function(xhr){
				filter.find('button').text('Processing...'); // changing the button label
				jQuery('.gallery-template .container').css("background-color","rgba(0,0,0,0.2)");
			},
			success:function(data){
				filter.find('button').text('Apply filter'); // changing the button label back
				jQuery('.gallery-template .container').css("background-color","#fff");
				$('#response').html(data); // insert data
				sliderShow();
			}
		});
		return false;
	});
});

});


</script>
<?php get_footer(); ?>
