<?php
/**
 * Handle the item terms shortcode.
 *
 * @link       https://bootstrapped.ventures
 * @since      3.0.0
 *
 * @package    WP_Ultimate_Post_Grid
 * @subpackage WP_Ultimate_Post_Grid/includes/public/shortcodes/item
 */

/**
 * Handle the item terms shortcode.
 *
 * @since      3.0.0
 * @package    WP_Ultimate_Post_Grid
 * @subpackage WP_Ultimate_Post_Grid/includes/public/shortcodes/item
 * @author     Brecht Vandersmissen <brecht@bootstrapped.ventures>
 */
class WPUPG_SC_Terms extends WPUPG_Template_Shortcode {
	public static $shortcode = 'wpupg-item-terms';

	public static function init() {
		$atts = array(
			'key' => array(
				'default' => '',
				'type' => 'text',
				'help' => 'Key of the taxonomy you want to display. Use "category" for regular categories and "post_tag" for regular tags.',
			),
			'display' => array(
				'default' => 'inline',
				'type' => 'dropdown',
				'options' => 'display_options',
			),
			'align' => array(
				'default' => 'left',
				'type' => 'dropdown',
				'options' => array(
					'left' => 'Left',
					'center' => 'Center',
					'right' => 'Right',
				),
				'dependency' => array(
					'id' => 'display',
					'value' => 'block',
				),
			),
			'text_style' => array(
				'default' => 'normal',
				'type' => 'dropdown',
				'options' => 'text_styles',
			),
			'term_separator' => array(
				'default' => ', ',
				'type' => 'text',
			),
		);

		$atts = array_merge( $atts, WPUPG_Template_Helper::get_label_container_atts() );

		self::$attributes = $atts;
		parent::init();
	}

	/**
	 * Output for the shortcode.
	 *
	 * @since	3.0.0
	 * @param	array $atts Options passed along with the shortcode.
	 */
	public static function shortcode( $atts ) {
		$atts = parent::get_attributes( $atts );

		$item = WPUPG_Template_Shortcodes::get_item();
		$terms = $item->terms( $atts['key'] );
		if ( ! $item || ! $terms || ! is_array( $terms ) ) {
			return '';
		}

		// Output.
		$classes = array(
			'wpupg-item-terms',
			'wpupg-block-text-' . $atts['text_style'],
		);

		// Alignment.
		if ( 'block' === $atts['display'] && 'left' !== $atts['align'] ) {
			$classes[] = 'wpupg-align-' . $atts['align']; 
		}
		
		// Term Output.
		$term_output = '';
		foreach ( $terms as $index => $term ) {
			if ( 0 !== $index ) {
				$term_output .= $atts['term_separator'];
			}
			$term_output .= is_object( $term ) ? $term->name : $term;
		}

		$label_container = WPUPG_Template_Helper::get_label_container( $atts, 'terms' );
		$tag = 'block' === $atts['display'] ? 'div' : 'span';
		$output = '<' . $tag . ' class="' . implode( ' ', $classes ) . '">' . $label_container . $term_output . '</' . $tag . '>';
		return apply_filters( parent::get_hook(), $output, $atts, $item );
	}
}

WPUPG_SC_Terms::init();